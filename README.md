# Terraform for GCP with GitLab CI.
A 101 tutorial for learning the basics of continuous intergration using Terraform for GCP.

See [Hashicorp Getting Started](https://learn.hashicorp.com/collections/terraform/gcp-get-started) for more Terraform basics.

## Requirements

- GCP free tier account. [Get started for free](https://cloud.google.com/) a CC is required to sign up, but no charges will be made even after the conclusion of the 90 day trial period, [more details](https://cloud.google.com/free/docs/gcp-free-tier).
- Docker for Desktop for [Windows](https://docs.docker.com/docker-for-windows/install/) or [Mac](https://docs.docker.com/docker-for-mac/install/).
- Terraform CLI [download](https://learn.hashicorp.com/tutorials/terraform/install-cli).
- A [GitLab account](https://gitlab.com/users/sign_in)

### Recommened 

- Completion of the [Hashicorp Getting Started](https://learn.hashicorp.com/collections/terraform/gcp-get-started).


## Setup

### Initialise Repository

- New Project > Create Blank Project > name repo > Create Project
- (Optional) Copy this README into your project for easy access.
- Create .gitignore using gitlab terraform template.
- Create .gitlab-ci.yml 

### GCP

To allow terraform to provision your infrastruction you need to set up the following.

- A GCP Project: GCP organizes resources into projects. [Create one now](https://console.cloud.google.com/projectcreate) in the GCP console. You'll need the Project ID later. You can see a list of your projects in the cloud resource manager.

## Tutorial

### Create simple Terraform Module

* Clone repository to your local machine.

Using your IDE of choice create the following directory structure (see [Hashicorp Getting Started](https://learn.hashicorp.com/collections/terraform/gcp-get-started) for more detail about the terraform module we are create here).

```
terraform-gcp-ci-101/
|_.git/
|_infrastructure/
| |_terraform/
|   |_main.tf
|   |_variables.tf
|_.gitignore
|_.gitlab-ci.yml
|_README.md
```
